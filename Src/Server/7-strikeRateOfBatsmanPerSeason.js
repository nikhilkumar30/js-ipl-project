const papa = require('papaparse');
const fs = require('fs');
const { match } = require("assert");
const path = require("path");

function convertMatches() {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
}

function convertDeliveries() {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/deliveries.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
}

function calculateStrikeRate(matchesData, deliveriesData) {
  const matchIdAndSeasons = {};

  // Create a mapping of match IDs to seasons
  for (let index = 0; index < matchesData.length; index++) {
    const match = matchesData[index];
    matchIdAndSeasons[match.id] = match.season;
  }

  const batsmanStrikeRate = {};

  for (let index = 0; index < deliveriesData.length; index++) {
    const delivery = deliveriesData[index];
    const batsman = delivery.batsman;
    const matchId = delivery.match_id;
    const season = matchIdAndSeasons[matchId];
    const totalRuns = parseInt(delivery.total_runs);
    const isWide = delivery.wide_runs !== '0';
    const isNoBall = delivery.noball_runs !== '0';

    if (!isWide && !isNoBall && season && ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017'].includes(season)) {
      if (!batsmanStrikeRate[season]) {
        batsmanStrikeRate[season] = {};
      }

      if (!batsmanStrikeRate[season][batsman]) {
        batsmanStrikeRate[season][batsman] = { runs: 0, balls: 0 };
      }

      batsmanStrikeRate[season][batsman].runs += totalRuns;
      batsmanStrikeRate[season][batsman].balls += 1;
    }
  }

  const result = {};

  for (const season in batsmanStrikeRate) {
    result[season] = {};

    for (const batsman in batsmanStrikeRate[season]) {
      const { balls } = batsmanStrikeRate[season][batsman];
      const strikeRate = balls > 0 ? ((batsmanStrikeRate[season][batsman].runs / balls) * 100).toFixed(2) : 0;
      result[season][batsman] = parseFloat(strikeRate);
    }
  }

  return result;
}

const isWrittenOrNot = (result) => {
  try {
    const fileName = "7-strikeRateOfBatsmanPerSeason.json";
    const resultFilePath = path.join(__dirname, "..", "Public/output", fileName);
    fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
  } catch {
    return "error";
  }
  return "Output file successfully written";
};

const matchesData = convertMatches();
const deliveriesData = convertDeliveries();
const result = calculateStrikeRate(matchesData, deliveriesData);

console.log(isWrittenOrNot(result));
