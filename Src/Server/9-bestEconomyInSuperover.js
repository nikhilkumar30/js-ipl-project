const papa = require('papaparse');
const fs = require('fs');
const path = require('path');

function convertDeliveries() {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/deliveries.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
}

function bestEconomyInSuperOvers(deliveriesData) {
  const economyMap = {};
  let bestEconomyBowler = null;
  let bestEconomy = null;

  for (let index = 0; index < deliveriesData.length; index++) {
    const delivery = deliveriesData[index];

    // Check if the delivery is part of a super over
    if (delivery.is_super_over === '1') {
      const bowler = delivery.bowler;
      const totalRuns = parseInt(delivery.batsman_runs) + parseInt(delivery.extra_runs) - parseInt(delivery.legbye_runs) - parseInt(delivery.bye_runs);
      const balls = 1; // Each delivery in a super over is considered a ball

      if (!economyMap[bowler]) {
        economyMap[bowler] = { runs: 0, balls: 0 };
      }

      economyMap[bowler].runs += totalRuns;
      economyMap[bowler].balls += balls;
    }
  }

  // Find the bowler with the best economy
  for (const bowler in economyMap) {
    const economy = (economyMap[bowler].runs / economyMap[bowler].balls) * 6;
    if (bestEconomyBowler === null || economy < bestEconomy) {
      bestEconomyBowler = bowler;
      bestEconomy = economy.toFixed(2);
    }
  }

  return { bestEconomyBowler, bestEconomy };
}

const isWrittenOrNot = (result) => {
  try {
    const fileName = '9-bestEconomyInSuperOvers_result.json';
    const resultFilePath = path.join(__dirname, '..', 'Public/output', fileName);
    fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8');
  } catch {
    return 'error';
  }
  return 'Output file successfully written';
};

const deliveriesData = convertDeliveries();
result = bestEconomyInSuperOvers(deliveriesData);
console.log(isWrittenOrNot(result));
