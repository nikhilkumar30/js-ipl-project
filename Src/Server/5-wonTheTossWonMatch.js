const papa = require('papaparse');
const fs = require('fs');
const { match } = require("assert");
const path = require("path");


const convertMatches = () => {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');

  const jsonData = papa.parse(fileContent, { header: true }).data;

  return jsonData;
};

function countTossAndMatchWins(matchesData) {
  const tossAndMatchWins = {};

  for (let index = 0; index < matchesData.length; index++) {
    const match = matchesData[index];
    const tossWinner = match.toss_winner;
    const matchWinner = match.winner;

    if (tossWinner && matchWinner && tossWinner === matchWinner) {
      if (!tossAndMatchWins[tossWinner]) {
        tossAndMatchWins[tossWinner] = 1;
      } else {
        tossAndMatchWins[tossWinner]++;
      }
    }
  }

  return tossAndMatchWins;
}

const isWrittenOrNot = (result) => {
    try {
      const fileName = "5-wonTheTossWonMatch.json";
      const resultFilePath = path.join(__dirname, "..", "Public/output", fileName);
      fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8');
    } catch {
      return "error";
    }
    return "Output file successfully written";
  };


const matchesData = convertMatches();
const result = countTossAndMatchWins(matchesData);
console.log(isWrittenOrNot(result));

