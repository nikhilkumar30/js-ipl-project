const papa = require("papaparse");
const fs = require("fs");
const { match } = require("assert");
const path = require("path");

const convert = () => {
  const filepath =
    "/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv";
  const fileContent = fs.readFileSync(filepath, "utf8");

  const jsonData = papa.parse(fileContent, { header: true }).data;

  return jsonData;
};

function countMatchesPerYear(jsonData) {
  const matchesPerYear = {};

  for (let index = 0; index < jsonData.length; index++) {
    const match = jsonData[index];
    const season = match.season;

    if (!matchesPerYear[season]) {
      matchesPerYear[season] = 1;
    } else {
      matchesPerYear[season]++;
    }
  }

  return matchesPerYear;
}

const isWrittenOrNot = (result) => {
  try {
    const fileName = "1-matchesPerYear_result.json";
    const resultFilePath = path.join(__dirname, "..", "Public/output", fileName);
    fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
  } catch {
    return "error";
  }
  return "Output file successfully written";
};

const jsonData = convert();
const result = countMatchesPerYear(jsonData);
console.log(isWrittenOrNot(result));
