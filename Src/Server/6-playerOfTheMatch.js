const papa = require('papaparse');
const fs = require('fs');
const { match } = require("assert");
const path = require("path");

const convertMatches = () => {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
};

function findHighestPlayerOfTheMatchAwards(matchesData) {
  const playerOfTheMatchAwards = {};

  // Loop through matches data
  for (let index = 0; index < matchesData.length; index++) {
    const match = matchesData[index];
    const season = match.season;
    const playerOfTheMatch = match.player_of_match;

    if (playerOfTheMatch) {
      // Initialize season's awards object if not exists
      if (!playerOfTheMatchAwards[season]) {
        playerOfTheMatchAwards[season] = {};
      }

      // Increment player's awards count for the season
      if (!playerOfTheMatchAwards[season][playerOfTheMatch]) {
        playerOfTheMatchAwards[season][playerOfTheMatch] = 1;
      } else {
        playerOfTheMatchAwards[season][playerOfTheMatch]++;
      }
    }
  }

  const highestAwardsPerSeason = {};

  // Loop through each season's awards
  for (const season in playerOfTheMatchAwards) {
    let highestPlayer = null;
    let highestCount = 0;

    // Loop through players in the season
    for (const player in playerOfTheMatchAwards[season]) {
      const awardsCount = playerOfTheMatchAwards[season][player];

      // Update highest player if found
      if (awardsCount > highestCount) {
        highestPlayer = player;
        highestCount = awardsCount;
      }
    }

    // Save the highest player and count for the season
    highestAwardsPerSeason[season] = { player: highestPlayer, awards: highestCount };
  }

  return highestAwardsPerSeason;
}

const isWrittenOrNot = (result) => {
    try {
      const fileName = "6-playerOfTheMatch_result.json";
      const resultFilePath = path.join(__dirname, "..", "Public/output", fileName);
      fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
    } catch {
      return "error";
    }
    return "Output file successfully written";
  };

const matchesData = convertMatches();
const result = findHighestPlayerOfTheMatchAwards(matchesData);
console.log(isWrittenOrNot(result));
