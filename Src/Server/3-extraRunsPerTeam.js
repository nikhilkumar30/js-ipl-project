const papa = require("papaparse");
const fs = require("fs");
const { match } = require("assert");
const path = require("path");

const convertMatches = () => {
  const filepath =
    "/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv";
  const fileContent = fs.readFileSync(filepath, "utf8");

  const jsonData = papa.parse(fileContent, { header: true }).data;

  return jsonData;
};

const convertDeliveries = () => {
  const filepath =
    "/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/deliveries.csv";
  const fileContent = fs.readFileSync(filepath, "utf8");

  const jsonData = papa.parse(fileContent, { header: true }).data;

  return jsonData;
};

function extraRunsConcededPerTeam(matchesData, deliveriesData, year) {
  const matchesInYear = [];

  for (let index = 0; index < matchesData.length; index++) {
    const match = matchesData[index];
    if (match.season === `${year}`) {
      matchesInYear.push(match.id);
    }
  }

  const extraRunsPerTeam = {};

  for (let index = 0; index < deliveriesData.length; index++) {
    const delivery = deliveriesData[index];
    const matchId = delivery.match_id;
    const extraRuns = Number(delivery.extra_runs);

    let isMatchInYear = false;
    for (let j = 0; j < matchesInYear.length; j++) {
      if (matchesInYear[j] === matchId) {
        isMatchInYear = true;
        break;
      }
    }

    if (isMatchInYear) {
      const bowlingTeam = delivery.bowling_team;

      if (!extraRunsPerTeam[bowlingTeam]) {
        extraRunsPerTeam[bowlingTeam] = 0;
      }

      extraRunsPerTeam[bowlingTeam] += extraRuns;
    }
  }

  return extraRunsPerTeam;
}

const isWrittenOrNot = (result) => {
  try {
    const fileName = "3-extraRunsPerTeam_result.json";
    const resultFilePath = path.join(
      __dirname,
      "..",
      "Public/output",
      fileName
    );
    fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
  } catch {
    return "error";
  }
  return "Output file successfully written";
};

const matchesData = convertMatches();
const deliveriesData = convertDeliveries();
const result = extraRunsConcededPerTeam(matchesData, deliveriesData, 2016);
// console.log(result);
console.log(isWrittenOrNot(result));
