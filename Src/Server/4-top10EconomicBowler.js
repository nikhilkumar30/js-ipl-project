const papa = require("papaparse");
const fs = require("fs");
const path = require("path");

const convertMatches = () => {
  const filepath =
    "/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/matches.csv";
  const fileContent = fs.readFileSync(filepath, "utf8");
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
};

const convertDeliveries = () => {
  const filepath =
    "/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/deliveries.csv";
  const fileContent = fs.readFileSync(filepath, "utf8");
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
};

function calculateEconomyRate(bowlingStats) {
  const overs =
    Math.floor(bowlingStats.balls / 6) + (bowlingStats.balls % 6) / 6;
  const economyRate = (bowlingStats.runs / overs).toFixed(2);
  return parseFloat(economyRate);
}

function topEconomicalBowlers(
  deliveriesData,
  matchesData,
  year,
  numberOfBowlers
) {
  const economicalBowlers = {};

  for (let index = 0; index < deliveriesData.length; index++) {
    const delivery = deliveriesData[index];
    const matchId = delivery.match_id;
    const bowler = delivery.bowler;

    let isMatchInYear = false;
    for (let j = 0; j < matchesData.length; j++) {
      const match = matchesData[j];
      if (match.id === matchId && match.season === `${year}`) {
        isMatchInYear = true;
        break;
      }
    }

    if (isMatchInYear) {
      if (!economicalBowlers[bowler]) {
        economicalBowlers[bowler] = {
          runs: 0,
          balls: 0,
        };
      }

      economicalBowlers[bowler].runs += parseInt(delivery.total_runs);
      economicalBowlers[bowler].balls += 1;
    }
  }

  const bowlersArray = [];
  for (const bowler in economicalBowlers) {
    const economyRate = calculateEconomyRate(economicalBowlers[bowler]);
    bowlersArray.push({ bowler, economyRate });
  }

  // Sorting the array using bubble sort (since loops are preferred)
  for (let i = 0; i < bowlersArray.length - 1; i++) {
    for (let j = 0; j < bowlersArray.length - i - 1; j++) {
      if (bowlersArray[j].economyRate > bowlersArray[j + 1].economyRate) {
        // Swap the elements
        const temp = bowlersArray[j];
        bowlersArray[j] = bowlersArray[j + 1];
        bowlersArray[j + 1] = temp;
      }
    }
  }

  const topBowlers = bowlersArray.slice(0, numberOfBowlers);

  return topBowlers;
}

const isWrittenOrNot = (result) => {
  try {
    const fileName = "4-top10EconomicBowler_result.json";
    const resultFilePath = path.join(
      __dirname,
      "..",
      "Public/output",
      fileName
    );
    fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
    return "Output file successfully written";
  } catch {
    return "Error";
  }
};

const matchesData = convertMatches();
const deliveriesData = convertDeliveries();
const result = topEconomicalBowlers(deliveriesData, matchesData, 2015, 10);
console.log(isWrittenOrNot(result));
