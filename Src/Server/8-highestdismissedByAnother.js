const papa = require('papaparse');
const fs = require('fs');
const { match } = require("assert");
const path = require("path");

function convertDeliveries() {
  const filepath = '/home/hp/Desktop/MountBlueProjects/IPL Project/Src/Data/deliveries.csv';
  const fileContent = fs.readFileSync(filepath, 'utf8');
  const jsonData = papa.parse(fileContent, { header: true }).data;
  return jsonData;
}

function numberOftimesDismmised(deliveriesData) {
  const dismissCount = {};

  for (let index = 0; index < deliveriesData.length; index++) {
    const obj = deliveriesData[index];
    const dismissPlayer = obj.player_dismissed;

    if (dismissPlayer) {
      if (!dismissCount[dismissPlayer]) {
        dismissCount[dismissPlayer] = 1;
      } else {
        dismissCount[dismissPlayer] += 1;
      }
    }
  }

  const dismissCountArray = Object.entries(dismissCount);

  // Sort the array based on the count of dismissals (in descending order)
  dismissCountArray.sort((a, b) => b[1] - a[1]);

  const highestDismissedPlayer = dismissCountArray[0] ? dismissCountArray[0][0] : null;
  const totalDismissals = dismissCountArray[0] ? dismissCountArray[0][1] : 0;

  return { highestDismissedPlayer, totalDismissals };
}

const isWrittenOrNot = (result) => {
    try {
      const fileName = "8-highestdismissedByAnother_result.json";
      const resultFilePath = path.join(__dirname, "..", "Public/output", fileName);
      fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), "utf8");
    } catch {
      return "error";
    }
    return "Output file successfully written";
  };

const deliveriesData = convertDeliveries();
result =numberOftimesDismmised(deliveriesData);
// console.log(result);
console.log(isWrittenOrNot(result));
